import ModalWindow from "./classes/ModalWindow.js";
import LoginForm from "./classes/LoginForm.js";
import SelectDoctor from "./classes/SelectDoctor.js";
import VisitCardiologist from "./classes/VisitCardiologist.js";
import VisitTherapist from "./classes/VisitTherapist.js";
import VisitDentist from "./classes/VisitDentist.js";
import Card from "./classes/Card.js";

import {
  LOCAL_STORAGE_TOKEN,
  CREATE_CARD_BUTTON,
  LOGIN_BUTTON,
  LOGOUT_BUTTON,
  CARDS_CONTAINER,
} from "./constants/constants.js";

import getToken from "./API/getToken.js";
import postVisit from "./API/postVisit.js";

import checkToken from "./functions/checkToken.js";
import showUserAccount from "./functions/showUserAccount.js";
import checkUserCards from "./functions/checkUserCards.js";
import renderFilterCard from "./functions/renderFilterCard.js";


const loginBtn = document.querySelector(LOGIN_BUTTON);
const logoutBtn = document.querySelector(LOGOUT_BUTTON);
const createCardBtn = document.querySelector(CREATE_CARD_BUTTON);

// Перевірка наявності авторизації при перезавантаженні сторінки ----------------
checkToken();

// авторизація користувача ------------------------------------------------------
loginBtn.addEventListener("click", () => {
  const form = new LoginForm();

  const onConfirmCallback = async (close) => {
    const body = form.getValues();
    const token = await getToken(body, form);

    if (token) {
      localStorage.setItem(LOCAL_STORAGE_TOKEN, token);
      close();
      showUserAccount();
    }
  };

  new ModalWindow(form.getFormElement(), "Вхід", "Увійти", onConfirmCallback).render();
});

// вихід з акаунту -------------------------------------------------------

logoutBtn.addEventListener('click', () => {
	if (localStorage.getItem(LOCAL_STORAGE_TOKEN)) {
		localStorage.removeItem(LOCAL_STORAGE_TOKEN);
		location.reload();
	};
});


// Поява модального вікна з вибором лікаря -------------------------------
// Поява модального вікна для заповнення пацієнтом -----------------------

createCardBtn.addEventListener("click", () => {
  let visit = null;

  const selectTypeDoctor = new SelectDoctor();

  selectTypeDoctor.selectDoctor.addEventListener("change", (e) => {
    if (visit) {
      visit.changeDoctor();
    }

    const doctor = e.target.value;

    if (doctor === "cardiologist") {
      visit = new VisitCardiologist("Кардіолог");
      visit.render(".modal-body");

    } else if (doctor === "dentist") {
      visit = new VisitDentist("Стоматолог");
      visit.render(".modal-body");

    } else if (doctor === "therapist") {
      visit = new VisitTherapist("Терапевт");
      visit.render(".modal-body");
    }
  });

  const confirmUser = async (close) => {
    const body = visit?.getValues();
    const response = await postVisit(body);

    if (response) {
      const { data } = response;
      await checkUserCards(); // виклик функції має бути тут у випадку перевірки карток на сервері

      new Card(data).render(CARDS_CONTAINER);
      // checkUserCards(); // виклик функції має бути тут у випадку перевірки карток в контейнері
      close();
    }
  };

  new ModalWindow(selectTypeDoctor.getFormElement(), "Створити візит", "Створити", confirmUser).render();
});


// Фільтрація карток -----------------------------------------------------

renderFilterCard();

// ------dragdrop

