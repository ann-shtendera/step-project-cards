import filterCard from "./filterCards.js";
import debounce from "../debounce/debounce.js";

const renderFilterCard = async () => {

    const inputSearch = document.querySelector(".filter__search-input");
    const selectStatus = document.querySelector(".filter__status-select");
    const selectPriority = document.querySelector(".filter__priority-select");
    
    inputSearch.addEventListener("input", debounce((event) => {
      filterCard(
         event.target.value,
        "search",
        inputSearch.value,
        selectStatus.value,
        selectPriority.value
      );
    }, 700));
    
    selectStatus.addEventListener("change", (event) => {
      filterCard(
        event.target.value,
        "status",
        inputSearch.value,
        selectStatus.value,
        selectPriority.value
      );
    });
    
    selectPriority.addEventListener("change", (event) => {
      filterCard(
        event.target.value,
        "priority",
        inputSearch.value,
        selectStatus.value,
        selectPriority.value
      );
    });
  }
  
export default renderFilterCard;

