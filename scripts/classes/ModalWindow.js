export default class ModalWindow {
    constructor (domElement, title, bntText, onConfirmFn) {
		this.domElement = domElement;
        this.title = title;
		this.bntText = bntText;
		this.onConfirmFn = onConfirmFn;
        this.modalWrapper = document.createElement('div');
        this.modalBackground = document.createElement('div');
        this.modalContent = document.createElement('div'); 
        this.modalContentHeader = document.createElement('div'); 
        this.modalContentBody = document.createElement('div'); 
        this.modalContentFooter = document.createElement('div'); 
        this.buttonClose = document.createElement('button');
		this.buttonConfirm = document.createElement('button');
    }

    createElement () {
        this.modalWrapper.className = 'modal modal__window';
        this.modalBackground.className = 'modal__background';
        this.modalContent.className = 'modal__main-container modal-content';
        this.modalContentHeader.className = 'modal-header';
        this.modalContentBody.className = 'modal-body';
        this.modalContentFooter.className = 'modal-footer';
        this.buttonClose.className = 'btn-close';
		this.buttonConfirm.className = "btn btn-info text-light";
  
        this.modalContentHeader.innerHTML = `<h1 class="modal-title fs-5">${this.title}</h1>`;
		this.buttonConfirm.innerText = this.bntText;
		this.buttonConfirm.type = 'button';
		
		this.modalContentHeader.append(this.buttonClose);
		this.modalContentBody.append(this.domElement);
		this.modalContentFooter.append(this.buttonConfirm);
        this.modalContent.append(this.modalContentHeader, this.modalContentBody, this.modalContentFooter);
        this.modalWrapper.append(this.modalBackground, this.modalContent);
    }

    addEventListeners() {
        this.buttonClose.addEventListener('click', () => {
            this.close();
        })
        this.modalBackground.addEventListener('click', () => {
            this.close();
        })
		this.buttonConfirm.addEventListener('click', () => {
            this.onConfirmFn(this.close.bind(this));
        })
    }

    close () {
        this.modalWrapper.remove();
    }

    render () {
        this.createElement();
        this.addEventListeners();
        document.body.append(this.modalWrapper);
    }
}

