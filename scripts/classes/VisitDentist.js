import Visit from "./Visit.js";

export default class VisitDentist extends Visit {
	constructor (doctor) {
        super(doctor);
    }

    createElements () {
        super.createElements();
    
        this.form.insertAdjacentHTML('beforeend', `
			<label for="date" class="ps-2">Дата останнього відвідування:</label>
			<input id="date" type="date" name="date" class="form-control" required>
		`);                
    }
}